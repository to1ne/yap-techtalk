% Created 2016-09-16 Fri 09:58
\documentclass[presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\RequirePackage{fancyvrb}
\DefineVerbatimEnvironment{verbatim}{Verbatim}{fontsize=\scriptsize}
\usepackage{minted}
\usemintedstyle{emacs}
\newminted{objc}{fontsize=\scriptsize}
\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
\usetheme{metropolis}
\author{Toon Claes}
\date{\today}
\title{YapDatabase on iOS}
\hypersetup{
 pdfauthor={Toon Claes},
 pdftitle={YapDatabase on iOS},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.1.50.1 (Org mode 8.3.5)},
 pdflang={English}}
\begin{document}

\maketitle

\section*{WhatDatabase?}
\label{sec:orgheadline5}
\begin{frame}[label={sec:orgheadline1}]{WhatDatabase? - YapDatabase!}
Source: \url{https://github.com/yapstudios/yapdatabase}
\begin{block}{SQLite}
YapDatabase is built atop SQLite.
\end{block}

\begin{block}{Key (\& collection) value store}
\begin{itemize}
\item key: The \emph{ID} of the object
\item value: Binary plist of the object
\item collection: Group of similar objects
\end{itemize}
\end{block}
\end{frame}

\begin{frame}[label={sec:orgheadline2}]{WhatDatabase? - Features}
\begin{description}
\item[{Concurrency}] Read simultaneously while another thread is writing.
\pause
\item[{Caching}] For faster access.
\pause
\item[{Views}] To filter, group \& sort data.
\pause
\item[{FTS}] Full Text Search, built atop SQLite's FTS module.
\pause
\item[{Relationships}] You can setup relationships between objects.
\pause
\item[{Extensions}] Extensions architecture built-in.
\end{description}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline3}]{WhatDatabase? - SQLite schema}
 \begin{center}
\begin{tabular}{rlrll}
rowid & collection & key & data & metadata\\
\hline
1 & Priority & 1 & \texttt{BLOB} & \texttt{NULL}\\
3 & Priority & 2 & \texttt{BLOB} & \texttt{NULL}\\
2 & Priority & 3 & \texttt{BLOB} & \texttt{NULL}\\
4 & Priority & 4 & \texttt{BLOB} & \texttt{NULL}\\
5 & Priority & 5 & \texttt{BLOB} & \texttt{NULL}\\
\end{tabular}
\end{center}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline4}]{WhatDatabase? - SQLite Data column}
 \begin{verbatim}
{
  "$version" => 100000
  "$objects" => [
    0 => "$null"
    1 => {
      "priorityDescription" => <CFKeyedArchiverUID 0x7ff01840eb10>{value = 2}
      "priorityId" => <CFKeyedArchiverUID 0x7ff01840eb30>{value = 3}
      "name" => <CFKeyedArchiverUID 0x7ff01840eb50>{value = 4}
      "isImmutable" => <CFKeyedArchiverUID 0x7ff01840eff0>{value = 5}
      "$class" => <CFKeyedArchiverUID 0x7ff01840f010>{value = 6}
    }
    2 => "needs to be handled within 24 hours"
    3 => "3"
    4 => "urgent"
    5 => 0
    6 => {
      "$classname" => "Priority"
      "$classes" => [
        0 => "Priority"
        1 => "DatabaseObject"
        2 => "NSObject"

    }

  "$archiver" => "NSKeyedArchiver"
  "$top" => {
    "root" => <CFKeyedArchiverUID 0x7ff01840f2d0>{value = 1}
  }
}
\end{verbatim}
\end{frame}

\section*{Basics}
\label{sec:orgheadline12}
\begin{frame}[fragile,label={sec:orgheadline6}]{Basics - Database}
 Create and/or Open the database file:

\begin{objccode}
YapDatabase *database = [[YapDatabase alloc] initWithPath:@"/path/to/database.sqlite"];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline7}]{Basics - Connection}
 Open a connection:

\begin{objccode}
YapDatabaseConnection *connection = [database newConnection];
\end{objccode}

An application can have multiple connections (for concurrency).
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline8}]{Basics - Writing}
 Writing data has to be done inside a read/write transaction:

\begin{objccode}
[connection readWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction){
  [transaction setObject:@"Hello" forKey:@"Greeter" inCollection:@"Words"];
}];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline9}]{Basics - Reading}
 Reading data can be done inside a read transaction:

\begin{objccode}
__block NSString *greeter = nil;
[connection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
  greeter = [transaction objectForKey:@"Greeter" inCollection:@"Words"]);
}];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline10}]{Basics - Storing objects}
 You can store any kind of object you want using YapDatabase!
\pause

It only needs to be \texttt{NSCoding} compliant.
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline11}]{Basics - Storing objects}
 \texttt{NSCoding} deserialization:

\begin{objccode}
- (id)initWithCoder:(NSCoder *)decoder {
  if ((self = [super init])) {
    myString   = [decoder decodeObjectForKey:@"myString"];
    myColor    = [decoder decodeObjectForKey:@"myColor"];
    myWhatever = [decoder decodeObjectForKey:@"myWhatever"];
    myFloat    = [decoder decodeFloatForKey:@"myFloat"];
  }
  return self;
}
\end{objccode}

\texttt{NSCoding} serialization:

\begin{objccode}
- (void)encodeWithCoder:(NSCoder *)encoder {
  [encoder encodeObject:myString   forKey:@"myString"];
  [encoder encodeObject:myColor    forKey:@"myColor"];
  [encoder encodeObject:myWhatever forKey:@"myWhatever"];
  [encoder encodeFloat:myFloat     forKey:@"myFloat"];
}
\end{objccode}
\end{frame}

\section*{Views}
\label{sec:orgheadline22}

\begin{frame}[fragile,label={sec:orgheadline13}]{Views - What are they?}
 \begin{itemize}
\item Do you want to display all your data, or just a subset of it?
\item Do you want to group it into sections?
\item How do you want to sort the objects?
\end{itemize}

In SQL this translates into:

\begin{itemize}
\item \texttt{WHERE} \ldots{} (filter)
\item \texttt{GROUP BY} \ldots{} (group)
\item \texttt{ORDER BY} \ldots{} (sort)
\end{itemize}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline14}]{Views - Understanding}
 Fill it with some data:

\begin{objccode}
[connection readWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction){
  [transaction setObject:goldfinger   forKey:@"abc123" inCollection:@"movies"];
  [transaction setObject:prettyInPink forKey:@"def456" inCollection:@"movies"];
  [transaction setObject:goldenEye    forKey:@"xyz123" inCollection:@"movies"];

  [transaction setObject:pierce forKey:@"abc123" inCollection:@"actors"];
  [transaction setObject:daniel forKey:@"xyz789" inCollection:@"actors"];
  [transaction setObject:sean   forKey:@"klm456" inCollection:@"actors"];
}];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline15}]{Views - Understanding}
 The sqlite database (in NSDictionary style markup):

\begin{objccode}
@{ @"database": @{
       @"movies": @{ @"abc123": <NFMovie:Goldfinger>,
                     @"def456": <NFMovie:Pretty In Pink>,
                     @"xyz123": <NFMovie:GoldenEye>, ...
                   },
       @"actors": @{ @"abc123": <NFActor:Pierce Brosnan>,
                     @"xyz789": <NFActor:Daniel Craig>,
                     @"klm456": <NFActor:Sean Connery>, ...
                   },
                 },
}
\end{objccode}

So to fetch the Goldfinger movie:

\begin{objccode}
[databaseConnection readWithBlock:^(YapDatabaseReadTransaction *transaction){
  NFMovie *goldfinger =
    [transaction objectForKey:@"abc123" inCollection:@"movies"];
}];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline16}]{Views - Grouping}
 Create grouping block object:

\begin{objccode}
YapDatabaseViewGrouping *grouping = [YapDatabaseViewGrouping withObjectBlock:
    ^NSString *(YapDatabaseReadTransaction *transaction,
                NSString *collection, NSString *key, id object)
{
  if ([object isBondMovie]) return @"bond movies";
  if ([object is80sMovie])  return @"80s movies";
  if ([object isBondActor]) return @"bond actors";

  return nil; // exclude from view
};
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline17}]{Views - Sorting}
 Create sorting block object:

\begin{objccode}
YapDatabaseViewSorting *sorting =
    ^(YapDatabaseReadTransaction *transaction, NSString *group,
      NSString *collection1, NSString *key1, id obj1,
      NSString *collection2, NSString *key2, id obj2)
{
  if ([obj1 isKindOfClass:[NFMovie class]])
    return [(NFMovie *)obj1 compareByReleaseDate:(NFMovie *)obj2];

  if ([obj1 isKindOfClass:[NFActor class]])
    return [(NFActor *)obj1 compareByName:(NFActor *)obj2];
};
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline18}]{Views - Create view}
 Create view with grouping and sorting block:

\begin{objccode}
YapDatabaseView *view =
    [[YapDatabaseView alloc] initWithGrouping:grouping
                                      sorting:sorting
                                   versionTag:@"007"];
\end{objccode}

And register view as an extension:

\begin{objccode}
[database registerExtension:view withName:@"all"];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline19}]{Views - Result}
 \begin{objccode}
@{ @"database": @{
     @"movies": @{ @"abc123": <NFMovie:Goldfinger>,
                   @"def456": <NFMovie:Pretty In Pink>,
                   @"xyz123": <NFMovie:GoldenEye>, ...
                 },
     @"actors": @{ @"abc123": <NFActor:Pierce Brosnan>,
                   @"xyz789": <NFActor:Daniel Craig>,
                   @"klm456": <NFActor:Sean Connery>, ...
                 },
               },
   @"view_all" : @{
     @"bond movies": @[ <@"movies", @"abc123">,   // <NFMovie:Goldfinger>
                        <@"movies", @"xyz123"> ], // <NFMovie:GoldenEye>

     @"80s movies" : @[ <@"movies", @"def456"> ], // <NFMovie:Pretty In Pink>

     @"bond actors": @[ <@"actors", @"xyz789">,   // <NFActor:Daniel Craig>
                        <@"actors", @"abc123">,   // <NFActor:Pierce Brosnan>
                        <@"actors", @"klm456"> ]  // <NFActor:Sean Connery>
                  }
}
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline20}]{Views - Lookup}
 To lookup an object from a view:

\begin{objccode}
NFMovie *goldfinger = [[transaction ext:@"all"] objectAtIndex:0 inGroup:@"bond movies"];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline21}]{Views - Mappings}
 Use mappings to select groups.

\begin{objccode}
YapDatabaseViewMappings *mappings =
  [YapDatabaseViewMappings mappingsWithGroups:@[ @"bond movies", @"bond actors" ]
                                         view:@"all"];
\end{objccode}
\pause

Or to reverse order.

\begin{objccode}
[mappings setIsReversed:YES forGroup:@"bond movies"];
\end{objccode}

So the first object is \texttt{goldeneye} (not \texttt{goldfinger}).

\begin{objccode}
NFMovie *goldeneye = [[transaction ext:@"myView"] objectAtRow:0 inSection:0 withMappings:mappings];
\end{objccode}
\end{frame}

\section*{Notifications}
\label{sec:orgheadline29}

\begin{frame}[fragile,label={sec:orgheadline23}]{Long-lived read transaction}
 Stable data-source that won't change until we tell it to.

\begin{objccode}
[databaseConnection beginLongLivedReadTransaction];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline24}]{Notifications - Register}
 Register for notifications when the database changes.

\begin{objccode}
[[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(yapDatabaseModified:)
                                             name:YapDatabaseModifiedNotification
                                           object:databaseConnection.database];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline25}]{Notifications - Call-back}
 Jump to the most recent commit.
End \& re-begin the long-lived transaction atomically.

\begin{objccode}
- (void)yapDatabaseModified:(NSNotification *)notification {
  NSArray *notifications = [databaseConnection beginLongLivedReadTransaction];

  // ...
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline26}]{Notifications - Get changes}
 \begin{objccode}
// ...

NSArray *sectionChanges = nil;
NSArray *rowChanges = nil;

[[databaseConnection ext:@"all"] getSectionChanges:&sectionChanges
                                        rowChanges:&rowChanges
                                  forNotifications:notifications
                                      withMappings:mappings];
// ...
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline27}]{Notifications - Animate Table View}
 \begin{objccode}
for (YapDatabaseViewRowChange *rowChange in rowChanges) {
  switch (rowChange.type) {
    case YapDatabaseViewChangeDelete:
      [self.tableView deleteRowsAtIndexPaths:@[ rowChange.indexPath ]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
      break;
    case YapDatabaseViewChangeInsert:
      [self.tableView insertRowsAtIndexPaths:@[ rowChange.newIndexPath ]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
      break;
    case YapDatabaseViewChangeMove:
      [self.tableView deleteRowsAtIndexPaths:@[ rowChange.indexPath ]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
      [self.tableView insertRowsAtIndexPaths:@[ rowChange.newIndexPath ]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
      break;
    case YapDatabaseViewChangeUpdate:
      [self.tableView reloadRowsAtIndexPaths:@[ rowChange.indexPath ]
                            withRowAnimation:UITableViewRowAnimationNone];
      break;
  }
}
\end{objccode}
\end{frame}

\begin{frame}[label={sec:orgheadline28}]{Notifications - Use case}
UI never interacts with API directly.

\usetikzlibrary{arrows,positioning,shapes}

\centering
\begin{tikzpicture}[node distance=1cm, auto]
  \tikzset{
    mythread/.style={rectangle,rounded corners,draw=black, top color=white, bottom color=yellow!50,very thick, inner sep=1em, minimum size=3em, text centered},
    mydb/.style={cylinder,draw=black, aspect=0.5, minimum height=80, minimum width=50, shape border rotate=90, top color=white, bottom color=green!50, very thick, text centered},
    myarrow/.style={<->, >=latex', shorten >=1pt, very thick}
  }
  \node[mydb] (yap) {Yap Database};

  \node[mythread, above=of yap] (ui) {UI Thread};
  \node[mythread, left=of yap] (network) {Network Thread};

  \draw[myarrow] (ui.south) -- (yap.north);
  \draw[myarrow] (network.east) -- (yap.west);
\end{tikzpicture}
\end{frame}

\section*{FTS}
\label{sec:orgheadline34}

\begin{frame}[fragile,label={sec:orgheadline30}]{FTS - Indexing}
 \begin{objccode}
YapDatabaseFullTextSearchBlockType blockType = YapDatabaseFullTextSearchBlockTypeWithObject;
YapDatabaseFullTextSearchWithObjectBlock block = ^(NSMutableDictionary *dict, NSString *collection,
                                                   NSString *key, id object) {
  if ([object isKindOfClass:[Tweet class]])
  {
    Tweet *tweet = (Tweet *)object;

    [dict setObject:tweet.author forKey:@"author"];
    [dict setObject:tweet.tweet forKey:@"tweet"];
  }
  else
  {
    // Don't need to index this item.
    // So we simply don't add anything to the dict.
  }
};
\end{objccode}

Indexing happens on-the-fly.
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline31}]{FTS - Create extension}
 Create FTS:

\begin{objccode}
NSArray *propertiesToIndexForMySearch = @[ @"author", @"tweet" ];

YapDatabaseFullTextSearch *fts =
  [[YapDatabaseFullTextSearch alloc] initWithColumnNames:propertiesToIndexForMySearch
                                                   block:block
                                               blockType:blockType]
\end{objccode}

And plug the extension into the database system:

\begin{objccode}
[database registerExtension:fts withName:@"fts"];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline32}]{FTS - Searching}
 \begin{objccode}
[connection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
  // Find matches for: "board meeting"
  [[transaction ext:@"fts"] enumerateKeysMatching:@"board meeting"
                                         usingBlock:^(NSString *collection, NSString *key, BOOL *stop) {
    // ...
    }];
}];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline33}]{FTS - Search examples}
 \begin{objccode}
[connection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
  // tweet.author matches: john
  // and row matches: board meeting
  [[transaction ext:@"fts"] enumerateKeysMatching:@"author:john board meeting"
                                       usingBlock:^(NSString *collection, NSString *key, BOOL *stop) {
    // ...
  }];

  // tweet.author matches: john
  // tweet.tweet contains phrase: "board meeting"
  [[transaction ext:@"fts"] enumerateKeysMatching:@"author:john tweet:\"board meeting\""
                                       usingBlock:^(NSString *collection, NSString *key, BOOL *stop) {
    // ...
  }];

  // find any tweets with the words "meeting" or "conference"
  [[transaction ext:@"fts"] enumerateKeysMatching:@"meeting OR conference"
                                       usingBlock:^(NSString *collection, NSString *key, BOOL *stop) {
    // ...
  }];
}];
\end{objccode}
\end{frame}


\section*{Relationships}
\label{sec:orgheadline39}

\begin{frame}[fragile,label={sec:orgheadline35}]{Relationships - Edges}
 Relations are defined as edges and there are two ways in which you can create an edge:

\begin{enumerate}
\item Manually
\item With the \texttt{YapDatabaseRelationshipNode} protocol
\end{enumerate}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline36}]{Relationships - Manual Edges}
 \begin{objccode}
[databaseConnection readWriteWithBlock:^(YapDatabaseReadWriteTransaction *transaction){
  YapDatabaseRelationshipEdge *edge =
  [YapDatabaseRelationshipEdge edgeWithName:@"bond"
                                  sourceKey:goldenEye.key
                                 collection:@"movies"
                             destinationKey:pierce.key
                                 collection:@"actors"
                            nodeDeleteRules:0];

  [[transaction ext:@"relationships"] addEdge:edge];
}];
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline37}]{Relationships - Protocol defined Edges}
 \begin{objccode}
- (NSArray *)yapDatabaseRelationshipEdges {
    YapDatabaseRelationshipEdge *bondEdge =
      [YapDatabaseRelationshipEdge edgeWithName:@"bond"
                                 destinationKey:pierce.key
                                     collection:@"actors"
                                nodeDeleteRules:0];
    return @[ bondEdge ];
}
\end{objccode}
\end{frame}

\begin{frame}[fragile,label={sec:orgheadline38}]{Relationships - Delete Rules}
 \begin{objccode}
typedef NS_OPTIONS(uint16_t, YDB_NodeDeleteRules) {
  // notify only
  YDB_NotifyIfSourceDeleted      = 1 << 0,
  YDB_NotifyIfDestinationDeleted = 1 << 1,

  // one-to-one
  YDB_DeleteSourceIfDestinationDeleted = 1 << 2,
  YDB_DeleteDestinationIfSourceDeleted = 1 << 3,

  // one-to-many & many-to-many
  YDB_DeleteSourceIfAllDestinationsDeleted = 1 << 4,
  YDB_DeleteDestinationIfAllSourcesDeleted = 1 << 5,
};
\end{objccode}

Use with care.
\end{frame}

\section*{Conclusions}
\label{sec:orgheadline41}

\begin{frame}[label={sec:orgheadline40}]{Conclusions}
\begin{itemize}
\item Easy to get started.
\pause
\item But hard to do right.
\pause
\item Key-Value-Collection.
\pause
\item No mandatory base class (only NSCoding).
\pause
\item Would use again? Maybe.
\end{itemize}
\end{frame}
\end{document}
