default: yap-ios.pdf

%.pdf: %.org
	emacs $< --batch -f org-beamer-export-to-pdf --kill

.PHONY: default
